import 'package:design_profile/profile.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _navToSignin();
  }

  _navToSignin() async {
    await Future.delayed(Duration(seconds: 3), () {});
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => Profile()));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'My App',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 24,
              color: Colors.grey[200]),
        ),
      ),
      backgroundColor: Colors.grey[700],
    );
  }
}
